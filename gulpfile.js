var gp = require('gulp');
var concat = require('gulp-concat');
var build = require('./static/vue/build/build');
var uglify = require('gulp-uglify');
var minifyCss = require('gulp-minify-css');                     //- 压缩CSS为一行；
var rev = require('gulp-rev');                                  //- 对文件名加MD5后缀
var revCollector = require('gulp-rev-collector');
gp.task("js",['build-vue'],function(){
    // 把1.js和2.js合并压缩为main.js，输出到dest/js目录下
    return gp.src(['./static/js/jquery-1.12.4.min.js',
        './static/js/Http.js',
        './static/js/as.js',
        './static/js/jquery.mousewheel.min.js',
        './static/js/jquery.mCustomScrollbar.min.js',
        './static/js/jquery.form.js',
        './static/js/pace.min.js',
        './static/js/particles.min.js',
        './static/js/codemirror.js',
        './static/js/highlight/xml.js',
        './static/js/plugins.js'

    ]).pipe(concat('athena.js')).pipe(uglify()).pipe(gp.dest('./src/main/resources/static/js'));
});
gp.task('css',['build-vue'], function() {
    return gp.src(['./static/css/bootstrap.min.css',
        './static/css/font-awesome.min.css',
        './static/css/pace-theme.css',
        './static/css/jquery.mCustomScrollbar.min.css',
        './static/css/common.css',
        './static/css/message.css',
        './static/css/codemirror.css',
        './static/vue/dist/css/app.css'
    ])
        .pipe(concat('athena.min.css'))                            //- 合并后的文件名
        .pipe(minifyCss())                                      //- 压缩处理成一行
        .pipe(gp.dest('./src/main/resources/static/css'));
});

gp.task("client-js",function(){
    // 把1.js和2.js合并压缩为main.js，输出到dest/js目录下
    return gp.src([
        './static/js/jquery-1.12.4.min.js',
        './static/js/jquery.form.js',
        './static/js/vue.min.js',
        './static/js/Http.js',
        './static/js/as.js',
        './static/js/jquery.mousewheel.min.js',
        './static/js/plugins.js'
    ]).pipe(concat('athena-client.js')).pipe(uglify()).pipe(gp.dest('./src/main/resources/static/js'));
});
gp.task('client-css', function() {
    return gp.src([ './static/css/message.css'])
        .pipe(concat('athena-client.min.css'))                            //- 合并后的文件名
        .pipe(minifyCss())                                      //- 压缩处理成一行
        .pipe(gp.dest('./src/main/resources/static/css'));
});

gp.task('copy-vue',['build-vue'], function() {
    return gp.src([ './static/vue/dist/js/app.js', './static/vue/dist/js/manifest.js', './static/vue/dist/js/vendor.js'])
        .pipe(gp.dest('./src/main/resources/static/js'));
});

gp.task('build-vue',function (cb) {
    build.start(cb);
});

gp.task('default', ['build-vue','copy-vue','js', 'css','client-js','client-css'],function () {
    console.log('default task')
});

gp.task('dev',function(){
    gp.watch('static/vue/src/**/*.vue',['dev-build']);
});

gp.task('dev-build',function (func) {
    build.dev(func);
});