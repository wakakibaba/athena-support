/**
 * Copyright (c) 2014.
 * Site:http://www.opdar.com
 * Created by Junfan.Shi on 2014/9/12 9:59.
 */

/**
 * @param url
 * @param data
 * @param callback
 * @constructor
 */
var HTTP = function(url,data,callback,error){
    if(typeof(data) === 'function'){
        error = callback;
        callback = data;
        data = {};
    }
    function XHR() {
        var xhr;
        try {xhr = new XMLHttpRequest();}
        catch(e) {
            var IEXHRVers =["Msxml3.XMLHTTP","Msxml2.XMLHTTP","Microsoft.XMLHTTP"];
            for (var i=0,len=IEXHRVers.length;i< len;i++) {
                try {xhr = new ActiveXObject(IEXHRVers[i]);}
                catch(e) {continue;}
            }
        }
        return xhr;
    }
    this.xhr = XHR();
    this.body = '';
    this.data = data;
    this.url = url;
    this.callback = callback;
    this.error = error;
    if(!this.error){
        this.error = function(e){

        }
    }

    this.setData(data);
};

HTTP.prototype.post = function(){
    var callback = this.callback;
    var error = this.error;
    var xhr = this.xhr;
    xhr.open("POST", this.url, true);
    xhr.onreadystatechange = function()
    {
        if(xhr.readyState==4 && xhr.status == 200){
            callback(xhr.responseText,xhr.status);
        }else if(xhr.readyState == 4 && xhr.status != 200){
            error(xhr);
        }
    };
    xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded; charset=utf-8');
    xhr.send(encodeURI(this.body));
};

HTTP.prototype.get = function(){
    var callback = this.callback;
    var xhr = this.xhr;
    var error = this.error;
    xhr.onreadystatechange = function()
    {
        if(xhr.readyState==4 && xhr.status == 200){
            callback(xhr.responseText);
        }else if(xhr.readyState == 4 && xhr.status != 200){
            error(xhr);
        }
    };
    xhr.open("GET",this.url+'?'+encodeURI(this.body),true);//当GET请求时，在地址栏中是带参数的，而参数为NULL，所以用get请求，send(null)
    xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded; charset=utf-8');
    xhr.send(null);
};


HTTP.prototype.postJSON = function(){
    var callback = this.callback;
    var xhr = this.xhr;
    var error = this.error;
    xhr.onreadystatechange = function()
    {
        if(xhr.readyState==4 && xhr.status == 200){
            callback(xhr.responseText,xhr.status);
        }else if(xhr.readyState == 4 && xhr.status != 200){
            error(xhr);
        }
    };
    xhr.open("POST",this.url,true);//当GET请求时，在地址栏中是带参数的，而参数为NULL，所以用get请求，send(null)
    xhr.setRequestHeader('Content-type', 'application/json;charset=utf-8');
    xhr.send(this.data);
};

HTTP.prototype.setData = function(data){
    this.data = data;
    for(var p in data){
        var v1 = data[p];
        if(v1 instanceof Array){
            for(var p2 in v1){
                var v2 = v1[p2];
                this.body = this.body + p+"="+v2+"&";
            }
        }else{
            if(v1 !== undefined && v1 !== ''){
                this.body = this.body + p+"="+v1+"&";
            }
        }
    }
    this.body=this.body.substring(0,this.body.length-1);
};