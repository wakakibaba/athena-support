package com.opdar.athena.support.mapper;

import com.opdar.athena.support.entities.AppEntity;
import com.opdar.athena.support.entities.UserEntity;
import com.opdar.plugins.mybatis.core.IBaseMapper;

/**
 * Created by shiju on 2017/7/11.
 */
public interface AppMapper extends IBaseMapper<AppEntity> {
}
