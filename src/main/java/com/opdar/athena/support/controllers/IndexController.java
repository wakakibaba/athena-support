package com.opdar.athena.support.controllers;

import com.opdar.athena.support.base.CommonInterceptor;
import com.opdar.athena.support.base.Constants;
import com.opdar.athena.support.base.ICacheManager;
import com.opdar.athena.support.entities.SupportUserEntity;
import com.opdar.athena.support.service.SupportUserService;
import com.opdar.athena.support.utils.qiniu.Auth;
import com.opdar.platform.annotations.Interceptor;
import com.opdar.platform.annotations.Request;
import com.opdar.platform.core.base.Context;
import com.opdar.platform.core.session.ISessionManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;

import javax.annotation.PostConstruct;
import java.util.UUID;

/**
 * Created by shiju on 2017/7/11.
 */
@Controller
public class IndexController {
    @Autowired
    SupportUserService userService;
    @Autowired
    ISessionManager<SupportUserEntity> sessionManager;
    @Autowired
    ICacheManager<String,String,Object> cacheManager;

    @Request(value = "/", format = Request.Format.VIEW)
    public String index(String userName,String userPwd){
        return "redirect:index.html";
    }

    @Request(value = "/login.html", format = Request.Format.VIEW)
    public String login(String userName,String userPwd){
        //login
        if(!StringUtils.isEmpty(userName) && !StringUtils.isEmpty(userPwd)){
            SupportUserEntity user = userService.login(userName, userPwd);
            if(user != null){
                String token = UUID.randomUUID().toString();
                Context.getRequest().getSession().setAttribute("token",token);
                String lastToken = (String) cacheManager.hget(Constants.LAST_TOKEN,user.getId());
                if(lastToken != null){
                    sessionManager.remove(lastToken);
                }
                sessionManager.set(token,user,24*60*60*1000);
                cacheManager.hset(Constants.USER_STATE,user.getId(),0);
                cacheManager.hset(Constants.LAST_TOKEN,user.getId(),token);
                return "redirect:index.html";
            }
        }
        return "login/index";
    }


    @Request(value = "/index.html", format = Request.Format.VIEW)
    public String index() {
        String token = (String) Context.getRequest().getSession().getAttribute("token");
        if (token == null) {
            return "redirect:login.html";
        }
        SupportUserEntity user = sessionManager.get(token);
        if (user == null) {
            Context.getRequest().getSession().removeAttribute("token");
            return "redirect:login.html";
        }
        return "support/index";
    }

    @Request(value = "/register.html", format = Request.Format.VIEW)
    public String register(String userName,String userPwd){
        //login
        if(!StringUtils.isEmpty(userName) && !StringUtils.isEmpty(userPwd)){
            SupportUserEntity user = userService.regist(userName, userPwd);
            if(user !=null){
                userService.init(user.getAppId());
                return "redirect:/login.html";
            }
        }
        return "login/register";
    }

    @Request(value = "/logout.html", format = Request.Format.VIEW)
    @Interceptor(CommonInterceptor.class)
    public String logout(){
        //login
        String token = (String) Context.getRequest().getSession().getAttribute("token");
        if(token != null){
            SupportUserEntity user = (SupportUserEntity) Context.getRequest().getAttribute("user");
            sessionManager.remove(token);
            cacheManager.hdel(Constants.USER_STATE,user.getId());
            cacheManager.hdel(Constants.LAST_TOKEN,user.getId());
            Context.getRequest().getSession().removeAttribute("token");
        }
        return "redirect:/login.html";
    }

}
